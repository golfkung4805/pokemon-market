import React, { ImgHTMLAttributes } from 'react';

import { imageFallback } from '../constants';

interface IImage extends React.DetailedHTMLProps<React.ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement> {}

const Image = ({ src, ...reset }: IImage) => {
  return (
    <img
      src={src || imageFallback}
      {...reset}
      onError={({ currentTarget }) => {
        currentTarget.onerror = null;
        currentTarget.src = imageFallback;
      }}
    />
  );
};

export default Image;
