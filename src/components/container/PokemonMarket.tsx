import React, { Fragment, useEffect, useMemo, useState } from 'react';
import ReactLoading from 'react-loading';

import { isEmpty } from 'lodash';

import { useCards } from '../../contexts/cards.context';
import { Each, cn, formatNumber } from '../../utils';
import CardItem from '../CardItem';
import Pagination from '../Pagination';
import CartMenu from '../cart/CartMenu';
import Select from '../form/Select';
import Navbar from '../layout/Navbar';

function PokemonMarket() {
  const { cards, filtersOptions, pagination, handleChangeFilter, handleChangePagination } = useCards();
  return (
    <div className="mx-auto max-w-screen-xl">
      <div className="flex h-dvh flex-col text-white transition-all duration-300">
        <Navbar />
        <div className={cn('relative flex-1')}>
          <div
            className={cn(
              'absolute inset-0 flex flex-col overflow-y-auto',
              'scrollbar-thin scrollbar-track-very-dark-gray scrollbar-thumb-light-gray scrollbar-track-rounded-full scrollbar-thumb-rounded-full',
            )}
          >
            <div
              className={cn(
                'flex flex-col items-center justify-between gap-3 p-5 filter sm:flex-row sm:gap-5',
                'static inset-x-0 top-0 z-10 bg-dark-gray sm:sticky',
              )}
            >
              <div className="min-w-36">
                <h2 className="text-lg font-semibold">Choose Card</h2>
                <span className="text-xs text-light-gray">
                  {!cards.isLoading ? (
                    <Fragment>
                      {formatNumber(pagination.startItem)} - {formatNumber(pagination.toItem)} of{' '}
                      {formatNumber(pagination.totalCount)}
                    </Fragment>
                  ) : (
                    `?? - ?? of ??`
                  )}{' '}
                  item
                </span>
              </div>
              <div className="xxs:w-auto xxs:flex-row xxs:gap-3 flex w-full flex-col gap-2">
                <Select
                  isLoading={filtersOptions.isLoading}
                  isDisabled={filtersOptions.isLoading}
                  options={filtersOptions.set}
                  onChangeValue={(newValue) => handleChangeFilter('set.id', newValue?.value)}
                />
                <Select
                  isLoading={filtersOptions.isLoading}
                  isDisabled={filtersOptions.isLoading}
                  options={filtersOptions.rarity}
                  onChangeValue={(newValue) => handleChangeFilter('rarity', newValue?.value)}
                />
                <Select
                  isLoading={filtersOptions.isLoading}
                  isDisabled={filtersOptions.isLoading}
                  options={filtersOptions.types}
                  onChangeValue={(newValue) => handleChangeFilter('types', newValue?.value)}
                />
              </div>
            </div>
            <div className="card-lists xs:grid-cols-2 grid auto-rows-min grid-cols-1 gap-5 p-5 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6">
              {cards.isLoading ? (
                <div className="col-span-6 flex justify-center">
                  <ReactLoading type="spokes" color="#ABBBC2" height="30%" width="30%" />
                </div>
              ) : !isEmpty(cards.lists) ? (
                Each({
                  of: cards.lists,
                  render: (cardInfo: any) => <CardItem cardInfo={cardInfo} />,
                })
              ) : (
                <h1 className="col-span-6 w-full text-center text-xl">Pokemon Cards Empty</h1>
              )}
            </div>
          </div>
        </div>
        {!isEmpty(cards.lists) ? (
          <Pagination
            containerClassName={cn(cards.isLoading && 'pointer-events-none opacity-50')}
            forcePage={pagination.page}
            pageCount={pagination.pageCount}
            onChange={handleChangePagination}
          />
        ) : null}
      </div>
      <CartMenu />
    </div>
  );
}

export default PokemonMarket;
