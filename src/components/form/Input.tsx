import React from 'react';

import { cn } from '../../utils';

interface IInput extends React.InputHTMLAttributes<HTMLInputElement> {
  rootClassName?: React.HTMLProps<HTMLElement>['className'];
  icon?: React.ReactNode;
}

const Input = ({ rootClassName, className, icon, readOnly, disabled, ...reset }: IInput) => {
  return (
    <div
      className={cn(
        'flex gap-2 rounded-lg border-[1px] border-gray-300 bg-dark-gray p-[14px]',
        'duration-300',
        (readOnly || disabled) && 'cursor-no-drop bg-white/[0.08]',
        !readOnly && 'focus-within:border-white hover:border-white',
        rootClassName,
      )}
    >
      {icon ? icon : null}
      <input
        readOnly={readOnly}
        disabled={disabled}
        className={cn(
          (readOnly || disabled) && 'cursor-no-drop',
          'w-full bg-transparent text-sm font-normal placeholder:text-light-gray focus:outline-none',
          className,
        )}
        {...reset}
      />
    </div>
  );
};

export default Input;
