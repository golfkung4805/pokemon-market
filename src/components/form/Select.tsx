import React, { useState } from 'react';
import { Props, default as ReactSelect } from 'react-select';

import { cn } from '../../utils';

export type SelectOptions = { value: string | number; label: string };

interface ISelect extends Props {
  onChangeValue?: (newValue: SelectOptions) => void;
}

const Select = ({ options, className, onChangeValue = () => {}, ...reset }: ISelect) => {
  const [menuOpen, setMenuOpen] = useState(false);

  const handleTriggerMenu = (status: boolean) => setMenuOpen(status);

  return (
    <ReactSelect
      menuIsOpen={menuOpen}
      onMenuOpen={() => handleTriggerMenu(true)}
      onMenuClose={() => handleTriggerMenu(false)}
      className={cn('custom-select', className)}
      isClearable
      classNames={{
        control: ({ isDisabled }) =>
          cn(
            '!bg-very-dark-gray !rounded-lg !border-gray-300 !shadow-none hover:!border-light-gray/80',
            isDisabled && '!opacity-50',
          ),
        input: () => '!min-w-12',
        singleValue: () => '!text-white',
        indicatorSeparator: () => '!hidden',
        clearIndicator: () => '!px-0',
        loadingIndicator: () => '!px-0 !mr-0',
        dropdownIndicator: ({ isFocused, ...reset }) => cn('!transition-all !duration-300', menuOpen && '!rotate-180'),
        menu: () => '!border-[1px] !border-gray-300 !rounded-lg overflow-hidden',
        menuList: () =>
          cn(
            '!bg-very-dark-gray !py-0',
            'scrollbar-thin scrollbar-track-very-dark-gray scrollbar-thumb-light-gray scrollbar-track-rounded-full scrollbar-thumb-rounded-full',
          ),
        option: ({ isSelected, isFocused }) =>
          cn(
            '!border-b-[1px] !border-gray-300 active:!bg-dark-gray',
            isFocused && '!bg-transparent',
            isSelected && '!bg-dark-gray',
          ),
        loadingMessage: () => '!text-white',
      }}
      onChange={(newValue) => {
        onChangeValue(newValue as SelectOptions);
      }}
      options={options}
      {...reset}
    />
  );
};
export default Select;
