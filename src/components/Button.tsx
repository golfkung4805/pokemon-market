import React from 'react';

import { cn } from '../utils';

const BUTTON_COLOR = {
  default: ['bg-white/[0.08]', 'hover:bg-white/[0.18] active:bg-white/[0.08]', 'disabled:bg-white/[0.08]'],
  primary: [
    'bg-primary hover:drop-shadow-primary active:bg-primary/80',
    'disabled:bg-primary/20 disabled:drop-shadow-none',
  ],
};

const BUTTON_SHAPE = {
  default: 'py-2 px-7',
  circle: 'h-12 w-12 p-1',
};

export interface IButton extends React.HTMLAttributes<HTMLButtonElement> {
  icon?: React.ReactNode;
  shape?: keyof typeof BUTTON_SHAPE;
  color?: keyof typeof BUTTON_COLOR;
}

const Button = ({ className, icon, color = 'default', shape = 'default', children, ...reset }: IButton) => {
  return (
    <button
      className={cn(
        'relative text-xs font-medium flex gap-2 rounded-lg items-center justify-center duration-300',
        'disabled:cursor-no-drop disabled:text-white/40',
        BUTTON_COLOR[color],
        BUTTON_SHAPE[shape],
        className,
      )}
      {...reset}
    >
      {icon ? <div>{icon}</div> : null}
      {children}
    </button>
  );
};

export default Button;
