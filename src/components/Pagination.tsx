import React from 'react';
import ReactPaginate, { ReactPaginateProps } from 'react-paginate';

import { useMediaQuery } from 'usehooks-ts';

import { cn } from '../utils';

const CLASS_BUTTON_PAGINATE =
  'bg-very-dark-gray rounded-full h-9 w-9 flex justify-center items-center border-[1px] border-white/20';

interface IPagination extends ReactPaginateProps {
  onChange: (newPage: number) => void;
}

const Pagination = ({ containerClassName, forcePage = 1, onChange = () => {}, ...reset }: Partial<IPagination>) => {
  const isMobile = useMediaQuery('(max-width: 425px)');
  const isTablet = useMediaQuery('(min-width: 768px)');

  return (
    <ReactPaginate
      previousLabel="<"
      nextLabel=">"
      forcePage={forcePage - 1}
      pageRangeDisplayed={isMobile ? 1 : 2}
      marginPagesDisplayed={isTablet ? 2 : 1}
      pageCount={1}
      containerClassName={cn(
        'flex jus gap-3 md:gap-5 justify-center items-center py-3 sm:py-5 px-5 border-t-[1px] border-white/[0.08]',
        containerClassName,
      )}
      nextLinkClassName={CLASS_BUTTON_PAGINATE}
      previousLinkClassName={CLASS_BUTTON_PAGINATE}
      pageLinkClassName={CLASS_BUTTON_PAGINATE}
      activeLinkClassName="!bg-primary"
      onPageChange={(selectedItem) => {
        onChange(selectedItem?.selected + 1);
      }}
      {...reset}
    />
  );
};

export default Pagination;
