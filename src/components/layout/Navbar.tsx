import React from 'react';

import SEARCH_ICON from '../../assets/icon/search.svg';
import { useCards } from '../../contexts/cards.context';
import { cn } from '../../utils';
import ButtonShoppingBag from '../ButtonShoppingBag';
import Image from '../Image';
import Input from '../form/Input';

const Navbar = () => {
  const { handleChangeFilter } = useCards();

  return (
    <div
      id="navbar"
      className={cn(
        'flex flex-col items-start justify-between gap-3 pb-6 sm:flex-row sm:items-center sm:gap-6',
        'w-full border-b-[1px] border-white/[0.08] p-5',
      )}
    >
      <div className="flex w-full items-center justify-between">
        <h1 className="text-[26px] font-semibold">Pokemon Market</h1>
        <ButtonShoppingBag className="flex flex-none sm:hidden" />
      </div>
      <div className="navbar-right flex w-full gap-4 sm:w-auto">
        <Input
          rootClassName="w-full sm:w-44"
          placeholder="Search by Name"
          icon={<Image src={SEARCH_ICON} className="h-5 w-5" alt="search-icon" />}
          onChange={(value) => handleChangeFilter('name', value.target.value)}
        />
        <ButtonShoppingBag className="hidden flex-none sm:flex" />
      </div>
    </div>
  );
};

export default Navbar;
