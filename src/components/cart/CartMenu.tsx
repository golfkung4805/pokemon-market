import React from 'react';
import Drawer from 'react-modern-drawer';

import { isEmpty } from 'lodash';

import X_ICON from '../../assets/icon/x.svg';
import { useCarts } from '../../contexts/carts.context';
import { Each, cn, formatNumber, formatPrice } from '../../utils';
import Button from '../Button';
import Image from '../Image';
import CartItem from './CartItem';

const CartMenu = () => {
  const { cartSummary, cartMenuOpen, carts, handleClearCart, handleTriggerCartMenu } = useCarts();
  return (
    <Drawer
      open={cartMenuOpen}
      onClose={handleTriggerCartMenu}
      direction="right"
      className="!w-full !bg-very-dark-gray text-white sm:!w-96"
    >
      <div className="flex h-full flex-col">
        <div className="header flex items-center justify-between p-5">
          <div className="flex flex-col">
            <h1 className="text-[26px]">Cart</h1>
            <span
              className="cursor-pointer text-sm text-light-gray underline hover:text-opacity-75 active:text-opacity-100"
              onClick={handleClearCart}
            >
              Clear all
            </span>
          </div>
          <Button
            color="primary"
            shape="circle"
            icon={<Image src={X_ICON} className="h-6 w-6" alt="close-icon" />}
            onClick={handleTriggerCartMenu}
          />
        </div>
        <div className="mb-2 flex justify-between px-5 text-sm">
          <samp className="w-14">Item</samp>
          <samp className="flex-1">Qty</samp>
          <samp>Price</samp>
        </div>
        <div
          className={cn(
            'mb-6 flex-1 space-y-6 overflow-y-auto overflow-x-hidden border-y-[1px] border-white/[0.08] p-5',
            'scrollbar-thin scrollbar-track-dark-gray scrollbar-thumb-light-gray scrollbar-track-rounded-full scrollbar-thumb-rounded-full',
          )}
        >
          {!isEmpty(carts) ? (
            Each({
              of: carts,
              render: (cartInfo) => <CartItem cartInfo={cartInfo} />,
            })
          ) : (
            <h1 className="text-center text-xl">Cart Empty</h1>
          )}
        </div>
        <div className="footer flex flex-col gap-4 px-5 pb-5 text-xs text-light-gray">
          <div className="cart-amount flex items-center justify-between">
            <h5>Total card amount</h5>
            <span className="text-base text-white">{formatNumber(cartSummary.cardAmount)}</span>
          </div>
          <div className="total-price flex items-center justify-between">
            <h5>Total price</h5>
            <span className="text-base text-white">{formatPrice(cartSummary.totalAmount)}</span>
          </div>
          <Button color="primary" className="w-full py-4 text-white">
            Continue to Payment
          </Button>
        </div>
      </div>
    </Drawer>
  );
};

export default CartMenu;
