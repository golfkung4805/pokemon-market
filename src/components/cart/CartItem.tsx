import React from 'react';

import { useCarts } from '../../contexts/carts.context';
import type { ICartItem as ICartItemInfo } from '../../contexts/carts.context';
import { formatNumber, formatPrice } from '../../utils';
import Button from '../Button';
import Image from '../Image';
import Input from '../form/Input';

interface ICartItem {
  cartInfo: ICartItemInfo;
}
const CartItem = ({ cartInfo }: ICartItem) => {
  const { handelAddQtyItem, handelMinusQtyItem } = useCarts();

  const { name, price, total = 0, qty = 0, image } = cartInfo;
  return (
    <div className="item-list-group flex flex-col gap-2">
      <div className="item-list flex gap-3">
        <Image src={image} className="h-[60px] w-[44px] flex-none" alt={name} />
        <div className="item-info flex-1 gap-1 text-xs">
          <h1 className="line-clamp-1 font-medium">{name}</h1>
          <samp className="text-nowrap font-poppins text-light-gray">{formatPrice(price)}</samp>
        </div>
        <h5 className="w-fit text-nowrap text-xs font-medium">{formatPrice(total)}</h5>
      </div>
      <div className="item-qty flex gap-2">
        <Button
          shape="circle"
          className="h-[54px] w-[54px] flex-none text-lg"
          onClick={() => handelMinusQtyItem(cartInfo)}
        >
          -
        </Button>
        <Input
          readOnly
          rootClassName="bg-white/[0.08] p-0 flex-1" // focus-within:bg-dark-gray
          className="w-full text-center text-lg leading-none"
          value={formatNumber(qty)}
        />
        <Button shape="circle" className="h-[54px] w-[54px] flex-none" onClick={() => handelAddQtyItem(cartInfo)}>
          +
        </Button>
      </div>
    </div>
  );
};

export default CartItem;
