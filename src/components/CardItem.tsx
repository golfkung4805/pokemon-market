import React from 'react';

import SHOPPING_BAG_ICON from '../assets/icon/shopping-bag.svg';
import { useCarts } from '../contexts/carts.context';
import type { ICardPokemon } from '../services/cards.service';
import { formatNumber, formatPrice } from '../utils';
import Button from './Button';
import Image from './Image';

interface ICardItem {
  cardInfo: ICardPokemon;
}

const CardItem = ({ cardInfo }: ICardItem) => {
  const { handelAddToCart } = useCarts();
  const { name, price = 0, image, qty } = cardInfo;

  return (
    <div className="xs:h-[280px] relative flex h-[407px] flex-col items-center justify-end">
      <Image src={image} className="xs:h-[142px] xs:w-[102px] absolute top-0 h-[270px] w-[194px]" alt={name} />
      <div className="card-info xs:h-[178px] flex h-[217px] w-full flex-col items-center justify-end space-y-2 rounded-2xl bg-very-dark-gray p-5">
        <h1 className="line-clamp-2 min-h-8 text-center text-xs font-medium">{name}</h1>
        <div className="flex gap-1 text-xs text-light-gray">
          <span>{formatPrice(price)}</span>•<span>{formatNumber(qty)} Cards</span>
        </div>
        <Button
          className="w-full px-0"
          icon={<Image src={SHOPPING_BAG_ICON} className="h-5 w-5" alt="search-icon" />}
          onClick={() => handelAddToCart(cardInfo)}
        >
          Add to cart
        </Button>
      </div>
    </div>
  );
};

export default CardItem;
