import React from 'react';

import SHOPPING_BAG_ICON from '../assets/icon/shopping-bag.svg';
import { useCarts } from '../contexts/carts.context';
import Button, { IButton } from './Button';
import Image from './Image';

const ButtonShoppingBag = (props: IButton) => {
  const { cartSummary, handleTriggerCartMenu } = useCarts();
  return (
    <Button
      color="primary"
      shape="circle"
      icon={<Image src={SHOPPING_BAG_ICON} className="h-5 w-5" alt="search-icon" />}
      {...props}
      onClick={handleTriggerCartMenu}
    >
      {cartSummary.cardAmount ? (
        <span className="item-card absolute -right-2 -top-2 flex h-5 items-center justify-center rounded-full bg-very-dark-gray p-1">
          {cartSummary.cardAmount > 99 ? '99+' : cartSummary.cardAmount}
        </span>
      ) : null}
    </Button>
  );
};

export default ButtonShoppingBag;
