import { createContext, useContext, useMemo, useState } from 'react';
import { toast } from 'react-toastify';

import numeral from 'numeral';

import type { ICardPokemon } from '../services/cards.service';

export interface ICartItem extends ICardPokemon {
  qty: number;
  total: number;
}

interface IDefaultValue {
  cartSummary: {
    cardAmount: number;
    totalAmount: number;
  };
  cartMenuOpen: boolean;
  carts: ICartItem[];
  handelAddToCart: (cardInfo: ICardPokemon) => void;
  handelAddQtyItem: (cardInfo: ICardPokemon) => void;
  handelMinusQtyItem: (cardInfo: ICardPokemon) => void;
  handleClearCart: () => void;
  handleTriggerCartMenu: () => void;
}

const defaultValue: IDefaultValue = {
  cartSummary: {
    cardAmount: 0,
    totalAmount: 0,
  },
  cartMenuOpen: false,
  carts: [],
  handelAddToCart: (cardInfo: ICardPokemon) => {},
  handelAddQtyItem: (cardInfo: ICardPokemon) => {},
  handelMinusQtyItem: (cardInfo: ICardPokemon) => {},
  handleClearCart: () => {},
  handleTriggerCartMenu: () => {},
};

const CartMenu = createContext(defaultValue as IDefaultValue);
export const CartMenuProvider = ({ children }: { children: JSX.Element }) => {
  const [cartMenuOpen, setCartMenuOpen] = useState(defaultValue.cartMenuOpen);
  const [carts, setCarts] = useState<ICartItem[]>(defaultValue.carts);

  const cartSummary = useMemo(() => {
    return carts.reduce((acc, curr) => {
      const newAcc = { ...acc };
      newAcc.cardAmount = Number(numeral(newAcc.cardAmount).add(curr?.qty).value());
      newAcc.totalAmount = Number(numeral(newAcc.totalAmount).add(curr?.total).value());
      return newAcc;
    }, defaultValue.cartSummary);
  }, [carts]);

  const handleTriggerCartMenu = () => setCartMenuOpen((prev) => !prev);

  const handelAddToCart = (cardInfo: ICardPokemon) => {
    try {
      const newCarts = [...carts];
      const indexCard = newCarts.findIndex((x) => x.id === cardInfo.id);
      if (indexCard !== -1) {
        handelAddQtyItem(cardInfo);
      } else {
        newCarts.push({ ...cardInfo, qty: 1, total: cardInfo.price });
        setCarts(newCarts);
        toast.success(`add ${cardInfo.name} to cart successfully`);
      }
    } catch (error) {
      toast.error(`can't add ${cardInfo.name} to cart`);
      console.error(error);
    }
  };

  const handelAddQtyItem = (cardInfo: ICardPokemon) => {
    try {
      const newCarts = [...carts];
      const indexCard = newCarts.findIndex((x) => x.id === cardInfo.id);
      if (indexCard !== -1) {
        newCarts[indexCard].qty = Number(numeral(newCarts[indexCard]?.qty).add(1).value());
        newCarts[indexCard].total = Number(numeral(newCarts[indexCard]?.total).add(newCarts[indexCard].price).value());
        setCarts(newCarts);
      } else {
        toast.warning(`can't add ${cardInfo.name}, because item not found.`);
      }
    } catch (error) {
      toast.error(`can't add qty item ${cardInfo.name}`);
      console.error(error);
    }
  };
  const handelMinusQtyItem = (cardInfo: ICardPokemon) => {
    try {
      const newCarts = [...carts];
      const indexCard = newCarts.findIndex((x) => x.id === cardInfo.id);
      if (indexCard !== -1) {
        newCarts[indexCard].qty = Number(numeral(newCarts[indexCard]?.qty).subtract(1).value());
        newCarts[indexCard].total = Number(
          numeral(newCarts[indexCard]?.total).subtract(newCarts[indexCard].price).value(),
        );
        if (newCarts[indexCard].qty <= 0) {
          newCarts.splice(indexCard, 1);
          toast.success(`delete item ${cardInfo.name} successfully`);
        } else {
        }
        setCarts(newCarts);
      } else {
        toast.warning(`can't minus ${cardInfo.name}, because item not found.`);
      }
    } catch (error) {
      toast.error(`can't minus qty item ${cardInfo.name}`);
      console.error(error);
    }
  };
  const handleClearCart = () => {
    setCarts([]);
    toast.success(`clear carts successfully`);
  };

  return (
    <CartMenu.Provider
      value={{
        cartSummary,
        carts,
        cartMenuOpen,
        handelAddToCart,
        handelAddQtyItem,
        handelMinusQtyItem,
        handleClearCart,
        handleTriggerCartMenu,
      }}
    >
      {children}
    </CartMenu.Provider>
  );
};
export const useCarts = () => useContext(CartMenu);
