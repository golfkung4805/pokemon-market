import { createContext, useContext, useEffect, useMemo, useState } from 'react';
import { toast } from 'react-toastify';

import { PokemonTCG } from 'pokemon-tcg-sdk-typescript';

import { SelectOptions } from '../components/form/Select';
import * as cardService from '../services/cards.service';
import type { ICardPokemon } from '../services/cards.service';
import * as typeService from '../services/types.service';
import { transferToOptions } from '../utils';

interface IDefaultValue {
  cards: {
    isLoading: boolean;
    lists: ICardPokemon[];
  };
  pagination: {
    page: number;
    pageSize: number;
    pageCount: number;
    totalCount: number;
    startItem: number;
    toItem: number;
  };
  filtersValue: {
    name: string;
    types: string;
    'set.id': string;
    rarity: string;
  };
  filtersOptions: {
    isLoading: boolean;
    types: SelectOptions[];
    set: SelectOptions[];
    rarity: SelectOptions[];
  };
  handleChangeFilter: (key: keyof IDefaultValue['filtersValue'], value: string | number) => void;
  handleChangePagination: (newPage: number) => void;
}

const defaultValue: IDefaultValue = {
  cards: { lists: [], isLoading: true },
  filtersValue: {
    name: '',
    types: '',
    'set.id': '',
    rarity: '',
  },
  pagination: {
    page: 1,
    pageSize: 20,
    pageCount: 5,
    totalCount: 100,
    startItem: 1,
    toItem: 20,
  },
  filtersOptions: {
    isLoading: true,
    types: [],
    set: [],
    rarity: [],
  },
  handleChangeFilter: (key: keyof IDefaultValue['filtersValue'], value: string | number) => {},
  handleChangePagination: (newPage: number) => {},
};

const Card = createContext(defaultValue as IDefaultValue);
export const CardProvider = ({ children }: { children: JSX.Element }) => {
  const [cards, setCards] = useState<IDefaultValue['cards']>(defaultValue.cards);
  const [filtersValue, setFilters] = useState<IDefaultValue['filtersValue']>(defaultValue.filtersValue);
  const [filtersOptions, setFiltersOptions] = useState<IDefaultValue['filtersOptions']>(defaultValue.filtersOptions);
  const [pagination, setPagination] = useState(defaultValue.pagination);

  const handleChangeFilter = (key: keyof IDefaultValue['filtersValue'], value: string | number) => {
    setCards((prev) => ({ ...prev, isLoading: true }));
    setFilters((prev) => ({ ...prev, [key]: value }));
    setPagination((prev) => ({
      ...prev,
      page: 1,
    }));
  };
  const handleChangePagination = (newPage: number) => {
    setCards((prev) => ({ ...prev, isLoading: true }));
    setPagination((prev) => ({ ...prev, page: newPage }));
  };

  const initCards = async (filter?: PokemonTCG.Parameter) => {
    try {
      setCards((prev) => ({ ...prev, isLoading: true }));
      const cards = await cardService.getAll(filter);
      setCards((prev) => ({ ...prev, lists: cardService.transferToCardLists(cards.data) }));
      setPagination((prev) => {
        const pageCount = Math.ceil(cards.totalCount / prev.pageSize);
        const startItem = (prev.page - 1) * prev.pageSize + 1;
        const toItem = prev.page * prev.pageSize;
        return {
          ...prev,
          totalCount: cards.totalCount,
          pageCount,
          startItem,
          toItem: toItem < cards.totalCount ? toItem : cards.totalCount,
        };
      });
    } catch (error) {
      toast.warning("can't fetch data card pokemon");
      console.error(error);
    } finally {
      setCards((prev) => ({ ...prev, isLoading: false }));
    }
  };
  const initFilterOptions = async () => {
    try {
      setFiltersOptions((prev) => ({ ...prev, isLoading: true }));
      const [types, set, rarity] = await Promise.all([
        typeService.getTypes(),
        typeService.getSet(),
        typeService.getRarity(),
      ]);
      setFiltersOptions((prev) => ({
        ...prev,
        set: transferToOptions(set, (item) => ({ label: item.name, value: item.id })),
        rarity: transferToOptions(rarity, (value) => ({ label: value, value: value })),
        types: transferToOptions(types, (value) => ({ label: value, value: value })),
      }));
    } catch (error) {
      toast.warning('cannot fetch data filter');
      console.error(error);
    } finally {
      setFiltersOptions((prev) => ({ ...prev, isLoading: false }));
    }
  };

  useEffect(() => {
    initFilterOptions();
  }, []);

  useEffect(() => {
    let timeout: NodeJS.Timeout;
    timeout = setTimeout(() => {
      let query = '';
      for (const key in filtersValue) {
        const _key = key as keyof IDefaultValue['filtersValue'];
        const value = filtersValue[_key];
        if (value) {
          if (_key === 'name') {
            query = query.concat(`${key}:"${value}*" `);
          } else {
            query = query.concat(`${key}:"${value}" `);
          }
        }
      }
      initCards({ ...pagination, q: query });
    }, 750);
    return () => clearTimeout(timeout);
  }, [pagination.page, pagination.pageSize, filtersValue]);

  return (
    <Card.Provider
      value={{ cards, filtersValue, filtersOptions, pagination, handleChangeFilter, handleChangePagination }}
    >
      {children}
    </Card.Provider>
  );
};
export const useCards = () => useContext(Card);
