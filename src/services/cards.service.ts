import axios from 'axios';
import { PokemonTCG } from 'pokemon-tcg-sdk-typescript';

import { ENDPOINT_CORE_API } from '../constants/env';
import { objectToURLSearchParams } from '../utils';

let controller = new AbortController();
const MODULE = '/cards';

type CardPrices = {
  averageSellPrice: number;
  lowPrice: number;
  trendPrice: number;
  germanProLow: number;
  suggestedPrice: number;
  reverseHoloSell: number;
  reverseHoloLow: number;
  reverseHoloTrend: number;
  lowPriceExPlus: number;
  avg1: number;
  avg7: number;
  avg30: number;
  reverseHoloAvg1: number;
  reverseHoloAvg7: number;
  reverseHoloAvg30: number;
};

interface ICardPokemonInfo extends PokemonTCG.Card {
  cardmarket: {
    url: string;
    updatedAt: string;
    prices: CardPrices;
  };
}

interface IResCardPokemonTCG {
  count: number;
  data: ICardPokemonInfo[];
  page: number;
  pageSize: number;
  totalCount: number;
}

export interface ICardPokemon {
  id: string;
  name: string;
  price: number;
  image: string;
  qty: number;
}
const transferToCardLists = (cards: ICardPokemonInfo[]): ICardPokemon[] =>
  cards.map(({ id, name, images, cardmarket, set }) => ({
    id,
    name,
    price: cardmarket?.prices?.averageSellPrice || 999,
    image: images.small,
    qty: set.total || 99,
  }));

const getAll = async (filter?: PokemonTCG.Parameter) => {
  controller.abort();
  if (controller.signal.aborted) {
    controller = new AbortController();
  }
  const resCards = await axios.get<IResCardPokemonTCG>(
    `${ENDPOINT_CORE_API}${MODULE}?${objectToURLSearchParams({ ...filter })}`,
    { signal: controller.signal },
  );
  return resCards.data;
};

export { getAll, transferToCardLists };
