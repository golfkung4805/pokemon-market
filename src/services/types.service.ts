import { PokemonTCG } from 'pokemon-tcg-sdk-typescript';

const getTypes = async () => {
  return await PokemonTCG.getTypes();
};

const getSet = async () => {
  return await PokemonTCG.getAllSets();
};

const getRarity = async () => {
  return await PokemonTCG.getRarities();
};

export { getTypes, getSet, getRarity };
