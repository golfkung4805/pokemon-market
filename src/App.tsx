import React, { Fragment } from 'react';
import { ToastContainer } from 'react-toastify';

import PokemonMarket from './components/container/PokemonMarket';
import { CardProvider } from './contexts/cards.context';
import { CartMenuProvider } from './contexts/carts.context';

const App = () => {
  return (
    <CardProvider>
      <CartMenuProvider>
        <Fragment>
          <PokemonMarket />
          <ToastContainer
            stacked
            position="top-center"
            autoClose={2000}
            hideProgressBar
            closeOnClick
            pauseOnHover={false}
            draggable={false}
            limit={5}
          />
        </Fragment>
      </CartMenuProvider>
    </CardProvider>
  );
};

export default App;
