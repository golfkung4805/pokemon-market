import React from 'react';

import { ClassValue, clsx } from 'clsx';
import numeral from 'numeral';
import { twMerge } from 'tailwind-merge';

interface IEach<T> {
  of: T[];
  render: (item: T, index: number) => React.ReactNode;
}

export const Each = <T>({ render, of }: IEach<T>) => {
  return React.Children.toArray(of.map((item, index: number) => render(item, index)));
};

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const formatNumber = (number: number) => {
  return numeral(number).format('0,0');
};

export const formatPrice = (price: number) => {
  return numeral(price).format('$ 0,0.00');
};

export const transferToOptions = <T, U>(arr: T[], transform: (item: T) => U): U[] => {
  return arr.map(transform);
};

export const objectToURLSearchParams = (object: any) => {
  return new URLSearchParams(object).toString();
};
