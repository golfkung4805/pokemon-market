/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,ts,jsx,tsx,mdx}'],
  theme: {
    extend: {
      screens: {
        xxxs: '320px',
        xxs: '375px',
        xs: '425px',
        sm: '576px',
        md: '768px',
        lg: '992px',
        xl: '1200px',
        xxl: '1400px',
        xxxl: '1600px',
      },
      colors: {
        primary: '#EA7C69',
        'light-gray': '#ABBBC2',
        'gray-300': '#393C49',
        'dark-gray': '#252836',
        'very-dark-gray': '#1F1D2B',
      },
      fontFamily: {
        poppins: ['Poppins'],
      },
      dropShadow: {
        primary: '0px 8px 24px rgba(234, 124, 105, 0.32)',
      },
    },
  },
  plugins: [require('tailwind-scrollbar')({ nocompatible: true, preferredStrategy: 'pseudoelements' })],
};
